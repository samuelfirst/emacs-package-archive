#!/bin/bash

tar xf "$1"
rm -rf "$1"
repo=$(sed s/.tar//g <<< "$1")
cd "$repo"
git pull origin main
NAME=$(grep 'define-package' *-pkg.el | awk '{print $2}' | sed 's/"//g')
VERSION=$(grep 'define-package' *-pkg.el | awk '{print $3}' | sed 's/"//g')
DESCRIPTION=$(grep 'define-package' *-pkg.el | awk -F '"' '{print "\""$6"\""}')
cd ..
mv "$repo" "$NAME-$VERSION"
tar cf "$NAME-$VERSION".tar "$NAME-$VERSION"
rm -rf "$NAME-$VERSION"
./rebuild-archive-contents.sh "$NAME" "$VERSION" "$DESCRIPTION"
