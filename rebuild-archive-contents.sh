#!/bin/bash

PACKAGE_NAME="$1"
PACKAGE_VERSION="$2"
PACKAGE_DESCRIPTION="$3"

PACKAGE="($PACKAGE_NAME . [($(sed 's/\./ /g' <<< $PACKAGE_VERSION)) nil $PACKAGE_DESCRIPTION tar nil])"

CONTENTS=$(sed -e "s/.*$PACKAGE_NAME.*//g" -e '/^$/d' archive-contents | head -n -1)

echo $CONTENTS > archive-contents
echo $PACKAGE >> archive-contents
echo ")" >> archive-contents
