#!/bin/bash

git clone "$1" new-repo
cd new-repo
NAME=$(grep 'define-package' *-pkg.el | awk '{print $2}' | sed 's/"//g')
VERSION=$(grep 'define-package' *-pkg.el | awk '{print $3}' | sed 's/"//g')
DESCRIPTION=$(grep 'define-package' *-pkg.el | awk -F '"' '{print "\""$6"\""}')
cd ..
mv new-repo "$NAME-$VERSION"
tar cf "$NAME-$VERSION".tar "$NAME-$VERSION"
rm -rf "$NAME-$VERSION"
./rebuild-archive-contents.sh "$NAME" "$VERSION" "$DESCRIPTION"
